





import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by MaksSklyarov on 13.04.17.
 */
public class ServerPNGThread extends Thread {
    private String name;
    private Socket socket;
    private ArrayList clientlist;


    public ServerPNGThread(String name, Socket socket, ArrayList clientlist){
        this.name = name;
        this.socket = socket;
        this.clientlist = clientlist;

    }

    @Override
    public void run(){
        long curTime = System.currentTimeMillis();
        Date curDate = new Date(curTime);
        String curStringDate = new SimpleDateFormat("dd.MM.yyyy").format(curDate);
        File folder = new File("tmp/"+curStringDate+name);
        folder.mkdirs();



        try {
            while(true){
            BufferedImage img = ImageIO.read(socket.getInputStream());
            if(img != null){
            ImageIO.write(img,"png",new File(folder+"/"+Math.random()*13+"img.png"));}

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }








}

