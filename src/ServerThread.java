import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Sklyarov on 06.04.2017.
 */
public class ServerThread extends Thread {
    private boolean registered = false;
    private String name;
    private Socket socket;
    private InputStreamReader inStream;
    private OutputStreamWriter outStream;
    private Scanner scanner;
    private String message;
    private PrintWriter out;
    private String myName;
    private ArrayList clientlist;
    public LogWriter logWriter;
    public ServerThread(String name, Socket socket, ArrayList clientlist){
        this.name = name;
        this.socket = socket;
        this.clientlist = clientlist;
        logWriter = new LogWriter(this.name+".txt");
    }
    @Override
    public void run(){
        try {
            BufferedImage img = ImageIO.read(socket.getInputStream());
            ImageIO.write(img,"png",new File(Math.random()*11+"img.png"));
            logWriter.Log("IP подконекченного - " + socket.getRemoteSocketAddress().toString());
            inStream = new InputStreamReader(socket.getInputStream());

        outStream = new OutputStreamWriter(socket.getOutputStream());
        scanner= new Scanner(inStream);
        out = new PrintWriter(outStream, true);

        System.out.println("Thread "+ name + " started" );
        logWriter.Log("Стартовали поток");
        Send("Welcome!\nСначала нужно обозначить свое имя.\nПрограмма не рассчитана на быдло - поэтому вот регексп синтаксиса данной команды '/register\\s\\w*");
        logWriter.Log("Отправили приветсвтие и инструкцию");
        String str;
        while (scanner.hasNextLine()){


                message=scanner.nextLine();//считываем его
                logWriter.Log("[" + new java.util.Date().toString()+"]" + "Получили смс:" + message);
            if(message.matches("/register\\s\\w*")){
                Send("Все супер!");
                logWriter.Log("[" + new java.util.Date().toString()+"]" + "Рега проканала");
                this.name = this.message.substring(10,this.message.length());
                this.registered = true;
                logWriter.rename(this.name + ".txt");
                logWriter.Log("IP подконекченного - " + socket.getRemoteSocketAddress().toString());

            }
            if(registered == true ){

                SendAll(this.name + ": " + message);//просим Control разослать сообщение всем
                logWriter.Log("[" + new java.util.Date().toString()+"]" + " Разослали всем  " + message);
                //System.out.println(message);//выводим в консоль




        }else{
                Send("Сначала регаемся!");
                logWriter.Log("Просим сначала зарегаться");
            }}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ServerThread getClient (int index){
        return ((ServerThread)clientlist.get(index));
    }
    public void Send(String s){//функция отправки клиенту сообщения
        out.println(s);
    }
    public  void SendAll(String message){
        for(int i=0;i<this.clientlist.size();i++){
            getClient(i).Send(message);
        }
    }
}
