/**
 * Created by MaksSklyarov on 10.04.17.
 */
import javax.xml.crypto.Data;
import java.io.*;
import java.sql.Time;

/**
 * Created by MaksSklyarov on 07.04.17.
 */
public class LogWriter {
    private File file;
    private FileWriter fw;
    private FileReader fr;
    private BufferedReader br;
    double bytes = 0;
    String fname;

    public LogWriter(String filename){
        fname = filename;
        file = new File(fname);
        bytes = file.length()/1024/1024;

    }
    public void Log(String string){
        try {
            if(this.bytes > 5.0){
                fname = fname + new java.util.Date().toString();
                file = new File(fname);
            }
            this.fw = new FileWriter(file,true);
            this.fr = new FileReader(file.getAbsoluteFile());
            this.br = new BufferedReader(fr);
            String str;
            StringBuilder sb = new StringBuilder();

            while((str = br.readLine())!= null){
                sb.append(str);
                sb.append("\n");
                //System.out.println(sb.toString());
            }

            fw.write(string + "\n");
            System.out.println(file.length());
            fw.close();

        }catch
                (IOException e) {
            e.printStackTrace();
        }
    }
    public void LogClose(){
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void rename(String filename){
        fname = filename;
        File fil1 = new File(filename);
        file.renameTo(fil1);
        file = new File(filename + ".txt");

    }

}

